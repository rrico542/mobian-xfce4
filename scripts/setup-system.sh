#!/bin/sh

# Setup hostname
echo $1 > /etc/hostname

# Generate locales (only en_US.UTF-8 for now)
sed -i -e '/en_US\.UTF-8/s/^# //g' /etc/locale.gen
locale-gen

# Change plymouth default theme
if plymouth-set-default-theme --list | grep -Fq 'mobian'; then    
  plymouth-set-default-theme mobian
fi

# Load phosh on startup, if it exists.
# Disable getty on tty1, as we won't connect in console mode anyway
if service --status-all | grep -Fq 'phosh'; then    
  systemctl enable phosh.service
  systemctl disable getty@.service
fi

